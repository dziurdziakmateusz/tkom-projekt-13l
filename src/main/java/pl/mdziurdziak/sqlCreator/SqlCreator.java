package pl.mdziurdziak.sqlCreator;

import java.io.IOException;
import java.io.OutputStream;

import pl.mdziurdziak.databaseStructure.Database;

/**
 * Interfejs klasy odpowiedzialnej za eksportowanie skryptów sql do strumieni
 * wyjściowych.
 * 
 * @see MsSqlCreator
 * @author Mateusz Dziurdziak
 */
public interface SqlCreator {
	public void checkForErrors(Database database) throws LogicException;

	/**
	 * Funkcja odpowiadająca za zapisanie skryptu sql do strumienia.
	 * 
	 * @param os
	 *            Strumień do którego mają być zapisywane dane. Funkcja
	 *            wywołująca musi go zamknąc.
	 * @param database
	 *            Baza danych, której struktura ma zostać wyeksportowana.
	 * @throws LogicException
	 *             Rzucany w przypadku błędu logicznego (np. słowo kluczowe jako
	 *             nazwa tabeli).
	 * @throws IOException
	 *             Rzucany w przypadku problemów ze strumieniem wyjściowym.
	 */
	public void write(OutputStream os, Database database)
			throws LogicException, IOException;
}
