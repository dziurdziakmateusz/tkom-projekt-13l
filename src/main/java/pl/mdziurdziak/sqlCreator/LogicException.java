package pl.mdziurdziak.sqlCreator;

/**
 * Wyjątek zwracany przez SqlCreatora związany z logiczną niezgodnością danych.
 * 
 * @see SqlCreator
 * @author Mateusz Dziurdziak
 */
public class LogicException extends Exception {
	private static final long serialVersionUID = 156638145951183794L;

	public LogicException(String string) {
		super(string);
	}
}
