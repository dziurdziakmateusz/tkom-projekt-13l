package pl.mdziurdziak.sqlCreator;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.List;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.databaseStructure.PrimaryKey;
import pl.mdziurdziak.databaseStructure.Table;

/**
 * Klasa eksportująca skrypt sql do tworzenia bazy danych zgodny z baza MS SQL.
 * 
 * @author Mateusz Dziurdziak
 */
public class MsSqlCreator implements SqlCreator {

	@Override
	public void checkForErrors(Database database) throws LogicException {
		MsSqlCreatorChecker.checkForErrors(database);
	}

	@Override
	public void write(OutputStream os, Database database)
			throws LogicException, IOException {
		generateConstraintName(database);
		checkForErrors(database);

		final Writer writer = new OutputStreamWriter(os);

		writeDatabaseHeader(writer, database);
		writeTables(writer, database.getTables());
		writeConstraints(writer, database);
		writer.flush();
	}

	private void generateConstraintName(Database database) {
		for (Table table : database.getTables()) {
			if (table.getPrimaryKey() != null
					&& table.getPrimaryKey().getName().equalsIgnoreCase("")) {
				table.getPrimaryKey().setName(table.getName() + "_PK");
			}
			int i = 1;
			for (Field field : table.getFields()) {
				if (field.getForeignKey() != null
						&& field.getForeignKey().getName().equalsIgnoreCase("")) {
					field.getForeignKey().setName(
							table.getName()
									+ "_"
									+ field.getForeignKey()
											.getReferencedTable() + "_" + i);
				}
				i++;
			}
		}
	}

	private void writeDatabaseHeader(Writer writer, Database database)
			throws IOException {
		writer.write("--database " + database.getName() + "\n\n");
	}

	private void writeTables(Writer writer, Collection<Table> tables)
			throws IOException {
		for (Table table : tables) {
			writeTableHeader(writer, table.getName());
			writeFields(writer, table.getFields());
			writeTableEnd(writer);
		}
	}

	private void writeConstraints(Writer writer, Database database)
			throws IOException {
		for (Table table : database.getTables()) {
			if (table.getPrimaryKey() != null) {
				writePrimaryKey(writer, table, table.getPrimaryKey());
			}
			for (Field field : table.getFields()) {
				if (field.getForeignKey() != null) {
					writeForeignKey(writer, table, field);
				}
			}
		}

	}

	private void writeForeignKey(Writer writer, Table table, Field field)
			throws IOException {
		writer.write("IF NOT EXISTS(SELECT name FROM sysobjects WHERE name = '"
				+ field.getForeignKey().getName()
				+ "' AND type = 'F')\nBEGIN\nALTER TABLE " + table.getName()
				+ "\n\tADD CONSTRAINT " + field.getForeignKey().getName()
				+ " FOREIGN KEY(\"" + field.getName() + "\")\n\tREFERENCES "
				+ field.getForeignKey().getReferencedTable() + "(\""
				+ field.getForeignKey().getReferencedField()
				+ "\")\nEND\nGO\n\n");
	}

	private void writePrimaryKey(Writer writer, Table table,
			PrimaryKey primaryKey) throws IOException {
		writer.write("IF NOT EXISTS(SELECT name FROM sysobjects WHERE name = '"
				+ primaryKey.getName()
				+ "' AND TYPE = 'PK')\nBEGIN\nALTER TABLE " + table.getName()
				+ "\n\tADD CONSTRAINT " + primaryKey.getName()
				+ " PRIMARY KEY(\"");
		for (String s : primaryKey.getFields()) {
			writer.write(s + " ");
		}
		writer.write("\")\nEND\nGO\n\n");
	}

	private void writeTableHeader(Writer writer, String tableName)
			throws IOException {
		writer.write("IF NOT EXISTS(SELECT name FROM sysobjects WHERE name = '"
				+ tableName + "' and type = 'I')\nBEGIN\nCREATE TABLE "
				+ tableName + "\n(\n");
	}

	private void writeFields(Writer writer, List<Field> fields)
			throws IOException {
		int i = 0;
		for (Field field : fields) {
			writeField(writer, field);
			if (i != fields.size() - 1) {
				writer.write(",");
			}
			writer.write("\n");
			i++;
		}
	}

	private void writeField(Writer writer, Field field) throws IOException {
		writer.write("\t" + field.getName() + "\t" + field.getType());
		if (field.isUnique()) {
			writer.write("\tUNIQUE");
		}
		if (field.isNotNull()) {
			writer.write("\tNOT NULL");
		}
	}

	private void writeTableEnd(Writer writer) throws IOException {
		writer.write(");\nEND\nGO\n\n");
	}
}
