package pl.mdziurdziak.sqlCreator;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.databaseStructure.ForeignKey;
import pl.mdziurdziak.databaseStructure.Table;
import pl.mdziurdziak.tokenizer.Token;
import pl.mdziurdziak.tokenizer.TokenIdentifier;
import pl.mdziurdziak.tokenizer.Tokenizer;
import pl.mdziurdziak.tokenizer.TokenizerImpl;

/**
 * Klasa sprawdzająca strukturę bazy danych pod kątem logiczności (odciąża klasę
 * MsSqlCreator).
 * 
 * @author Mateusz Dziurdziak
 */
class MsSqlCreatorChecker {
	private MsSqlCreatorChecker() {
	}

	public static void checkForErrors(Database database) throws LogicException {
		checkNotNull(database, "Database cannot be null");
		checkTableNamesDuplicates(database.getTables());
		Map<String, Object> keywords = null;
		try {
			keywords = getSqlKeywords();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (keywords != null) {
			checkIfTableNamesAreKeywords(database.getTables(), keywords);
		}

		for (Table table : database.getTables()) {
			checkIfTableEmpty(table);
			checkFieldsNamesDuplicates(table.getFields());
			if (keywords != null) {
				checkIfFieldNamesAreKeywords(table.getFields(), keywords);
			}
			checkPrimaryKey(table);
		}

		checkForeignKeys(database);
	}

	private static void checkTableNamesDuplicates(Collection<Table> tables)
			throws LogicException {
		final Map<String, Object> tableNames = new HashMap<>();
		for (Table table : tables) {
			if (tableNames.get(table.getName()) != null) {
				throw new LogicException("Duplicate table name: "
						+ table.getName());
			}

			tableNames.put(table.getName(), true);
		}
	}

	private static Map<String, Object> getSqlKeywords() throws IOException {
		final Map<String, Object> keywords = new HashMap<>();
		final Tokenizer tokenizer = new TokenizerImpl();
		tokenizer.setInputStream(new FileInputStream("mssqlkeywords.txt"));
		for (;;) {
			Token token = tokenizer.getNextToken();
			if (token.getId() == TokenIdentifier.SIMPLE_STRING) {
				keywords.put(token.getValue(), true);
			} else if (token.getId() == TokenIdentifier.EOF) {
				break;
			}
		}
		((TokenizerImpl) tokenizer).getInputStream().close();
		return keywords;
	}

	private static void checkIfTableNamesAreKeywords(Collection<Table> tables,
			Map<String, Object> keywords) throws LogicException {

		for (Table table : tables) {
			if (keywords.get(table.getName()) != null) {
				throw new LogicException(table.getName() + " is sql keywords.");
			}
		}
	}

	private static void checkIfTableEmpty(Table table) throws LogicException {
		if (table.getFields().size() == 0) {
			throw new LogicException("Table " + table.getName() + " is empty.");
		}
	}

	private static void checkFieldsNamesDuplicates(List<Field> fields)
			throws LogicException {
		final Map<String, Object> fieldNames = new HashMap<>();
		for (Field field : fields) {
			if (fieldNames.get(field.getName()) != null) {
				throw new LogicException("Duplicate field name: "
						+ field.getName());
			}

			fieldNames.put(field.getName(), true);
		}
	}

	private static void checkIfFieldNamesAreKeywords(List<Field> fields,
			Map<String, Object> keywords) throws LogicException {
		for (Field field : fields) {
			if (keywords.get(field.getName()) != null) {
				throw new LogicException(field.getName() + " is sql keywords.");
			}
		}
	}

	private static void checkPrimaryKey(Table table) throws LogicException {
		if (table.getPrimaryKey() == null) {
			return;
		}
		checkPrimaryKeyFieldsDuplicates(table.getPrimaryKey().getFields());
		checkPrimaryKeyFieldsExistence(table);
		checkIfFieldsAreNotNullAndUnique(table.getPrimaryKey().getFields(),
				table);
	}

	private static void checkIfFieldsAreNotNullAndUnique(Vector<String> fields,
			Table table) throws LogicException {
		for (String fieldName : fields) {
			final Field field = table.getFieldByName(fieldName);
			if (!field.isNotNull() || !field.isUnique()) {
				throw new LogicException("Field " + fieldName + " in table "
						+ table.getName() + " should be unique and not null");
			}
		}
	}

	private static void checkPrimaryKeyFieldsExistence(Table table)
			throws LogicException {
		final Map<String, Object> fieldNames = new HashMap<>();
		for (Field field : table.getFields()) {
			fieldNames.put(field.getName(), true);
		}

		for (String pkField : table.getPrimaryKey().getFields()) {
			if (fieldNames.get(pkField) == null) {
				throw new LogicException("Field " + pkField
						+ " not definied in table " + table.getName());
			}
		}
	}

	private static void checkPrimaryKeyFieldsDuplicates(List<String> fields)
			throws LogicException {
		final Map<String, Object> names = new HashMap<>();
		for (String name : fields) {
			if (names.get(name) != null) {
				throw new LogicException("Duplicate primary key field name: "
						+ name);
			}

			names.put(name, true);
		}
	}

	private static void checkForeignKeys(Database database)
			throws LogicException {
		for (Table table : database.getTables()) {
			for (Field field : table.getFields()) {
				if (field.getForeignKey() != null) {
					final ForeignKey fk = field.getForeignKey();
					final Table refTable = database.getTableByName(fk
							.getReferencedTable());
					if (refTable == null) {
						throw new LogicException("Table "
								+ fk.getReferencedTable() + " not defined.");
					}
					final Field refField = refTable.getFieldByName(fk
							.getReferencedField());
					if (refField == null) {
						throw new LogicException("Field "
								+ fk.getReferencedField() + " in table "
								+ refTable.getName() + " not defined.");
					}
					if (!refField.getType().equalsIgnoreCase(field.getType())) {
						throw new LogicException(table.getName() + "."
								+ field.getName() + " and "
								+ refTable.getName() + "." + refField.getName()
								+ " - diffrent types.");
					}
				}
			}
		}
	}
}
