package pl.mdziurdziak.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import pl.mdziurdziak.databaseStructure.PrimaryKey;
import pl.mdziurdziak.databaseStructure.Table;

class TablePanel extends JPanel {
	private static final long serialVersionUID = -2820324186847251107L;

	private MyComboBoxModel<Table> tableListModel;
	private JComboBox<Table> tablesList;
	private JButton addTableButton;
	private JButton modifyTableButton;
	private JButton deleteTableButton;
	private List<TablePanelObserver> observers;

	private JTextField primaryKeyNameTextField;
	private MyListModel<String> primaryKeyFieldsModel;
	private JList<String> primaryKeyFields;
	private JButton addPkField;
	private JButton deletePkField;

	private JPanel panel;

	public TablePanel(Vector<Table> tables) {
		observers = new LinkedList<>();
		setLayout(new GridLayout(1, 1));
		panel = new JPanel();
		initializeComponents(tables);
		prepareLayout();
		add(panel);
	}

	public Table getSelectedTable() {
		if (tablesList.getSelectedIndex() == -1) {
			return null;
		}

		return tableListModel.get(tablesList.getSelectedIndex());
	}

	public void addObserver(TablePanelObserver observer) {
		observers.add(observer);
	}

	public void removeObserver(TablePanelObserver tablePanelObserver) {
		observers.remove(tablePanelObserver);
	}

	private void initializeComponents(Vector<Table> tables) {
		tableListModel = new MyComboBoxModel<>(tables);
		tablesList = new JComboBox<>(tableListModel);
		tablesList.setMinimumSize(new Dimension(100, 40));
		tablesList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (TablePanelObserver tbo : observers) {
					tbo.notifyObserver();
				}

				if (tablesList.getSelectedIndex() == -1) {
					primaryKeyNameTextField.setText("");
					primaryKeyNameTextField.setEnabled(false);
					primaryKeyFields.setListData(new Vector<String>());
					primaryKeyFields.setEnabled(false);
					addPkField.setEnabled(false);
					deletePkField.setEnabled(false);
					return;
				}

				primaryKeyNameTextField.setEnabled(true);
				primaryKeyFields.setEnabled(true);
				Table t = (Table) tableListModel.getSelectedItem();
				if (t.getPrimaryKey() != null) {
					primaryKeyNameTextField
							.setText(t.getPrimaryKey().getName());
					primaryKeyFieldsModel.setVector(t.getPrimaryKey()
							.getFields());
				} else {
					primaryKeyNameTextField.setText("");
					primaryKeyFieldsModel.setVector(new Vector<String>());
				}
				addPkField.setEnabled(true);
				deletePkField.setEnabled(true);
			}
		});

		addTableButton = new JButton("Add");
		addTableButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				final TableEditor tableEditor = new TableEditor();
				final Table t = tableEditor.runAdd();
				if (t != null) {
					tableListModel.add(t);
				}
			}
		});

		modifyTableButton = new JButton("Modify");
		modifyTableButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				if (tablesList.getSelectedIndex() == -1) {
					return;
				}
				final TableEditor tableEditor = new TableEditor();
				final Table t = tableEditor.runModify(tableListModel
						.get(tablesList.getSelectedIndex()));
				if (t != null) {
					tableListModel.modify(t, tablesList.getSelectedIndex());
				}
			}
		});

		deleteTableButton = new JButton("Delete");
		deleteTableButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				if (tablesList.getSelectedIndex() == -1) {
					return;
				}
				tableListModel.remove(tablesList.getSelectedIndex());
			}
		});

		primaryKeyNameTextField = new JTextField();
		primaryKeyNameTextField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tablesList.getSelectedIndex() == -1) {
					return;
				}
				Table t = (Table) tableListModel.getSelectedItem();
				if(t.getPrimaryKey() != null){
					t.getPrimaryKey().setName(primaryKeyNameTextField.getText());
				}
				else{
					t.setPrimaryKey(new PrimaryKey(primaryKeyNameTextField.getName()));
				}
			}
		});

		primaryKeyFieldsModel = new MyListModel<>();
		primaryKeyFields = new JList<>(primaryKeyFieldsModel);

		addPkField = new JButton("Add PK field");
		addPkField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tablesList.getSelectedIndex() == -1) {
					return;
				}
				StringGetter sg = new StringGetter("Insert field name");
				String name = sg.getString();
				if (name == null) {
					return;
				}
				Table t = (Table) tableListModel.getSelectedItem();
				if(t.getPrimaryKey() == null)
				{
					t.setPrimaryKey(new PrimaryKey(""));
					primaryKeyFieldsModel.setVector(t.getPrimaryKey().getFields());
				}
				primaryKeyFieldsModel.addElement(name);
			}
		});
		addPkField.setEnabled(false);

		deletePkField = new JButton("Delete PK field");
		deletePkField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tablesList.getSelectedIndex() == -1) {
					return;
				}
				if (primaryKeyFields.getSelectedIndex() == -1) {
					return;
				}
				primaryKeyFieldsModel.remove(primaryKeyFields
						.getSelectedIndex());
				if(primaryKeyFieldsModel.getSize() == 0){
					((Table) tableListModel.getSelectedItem()).setPrimaryKey(null);
					primaryKeyFieldsModel.setVector(new Vector<String>());
				}
			}
		});
		deletePkField.setEnabled(false);
	}

	private void prepareLayout() {
		panel.setLayout(new GridLayout(2, 1));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 4));
		buttonPanel.add(tablesList);
		buttonPanel.add(addTableButton);
		buttonPanel.add(modifyTableButton);
		buttonPanel.add(deleteTableButton);
		panel.add(buttonPanel);

		JPanel pkPanel = new JPanel();
		pkPanel.setLayout(new GridLayout(1, 5));
		pkPanel.add(new JLabel("PK name"));
		pkPanel.add(primaryKeyNameTextField);
		pkPanel.add(new JScrollPane(primaryKeyFields));
		pkPanel.add(addPkField);
		pkPanel.add(deletePkField);
		panel.add(pkPanel);
	}

	private class MyListModel<E> implements ListModel<E> {

		private Vector<E> data = new Vector<>();
		private List<ListDataListener> listeners = new LinkedList<>();

		@Override
		public int getSize() {
			return data.size();
		}

		@Override
		public E getElementAt(int index) {
			return data.get(index);
		}

		@Override
		public void addListDataListener(ListDataListener l) {
			listeners.add(l);
		}

		@Override
		public void removeListDataListener(ListDataListener l) {
			listeners.remove(l);
		}

		public void setVector(Vector<E> data) {
			this.data = data;
			for (ListDataListener l : listeners) {
				l.intervalRemoved(new ListDataEvent(this,
						ListDataEvent.INTERVAL_REMOVED, 0, data.size()));
			}
		}

		public void addElement(E toAdd) {
			data.add(toAdd);
			for (ListDataListener l : listeners) {
				l.intervalAdded(new ListDataEvent(this,
						ListDataEvent.INTERVAL_ADDED, data.size() - 1, data
								.size() - 1));
			}
		}

		public void remove(int index) {
			data.remove(index);
			for (ListDataListener l : listeners) {
				l.intervalRemoved(new ListDataEvent(this,
						ListDataEvent.INTERVAL_REMOVED, index, index));
			}
		}

	}
}
