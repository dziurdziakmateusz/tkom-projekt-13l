package pl.mdziurdziak.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.databaseStructure.ForeignKey;

public class FieldPanel extends JPanel {
	private static final long serialVersionUID = 8879701119248883979L;

	private MyComboBoxModel<Field> fieldListModel;
	private JComboBox<Field> fieldList;
	private JButton addFieldButton;
	private JButton modifyFieldButton;
	private JButton deleteFieldButton;
	private JCheckBox uniqueCheckBox;
	private JCheckBox notNullCheckBox;
	private JTextField foreignKeyNameTextField;
	private JTextField foreignKeyRefTableTextField;
	private JTextField foreignKeyRefFieldTextField;

	private JPanel panel;

	public FieldPanel(Vector<Field> fields) {
		setLayout(new GridLayout(1, 1));
		panel = new JPanel();
		initializeComponents(fields);
		prepareLayout();
		add(panel);
	}

	public void setFields(Vector<Field> fields) {
		panel.removeAll();
		initializeComponents(fields);
		prepareLayout();
		panel.validate();
	}

	public void setEnabled(boolean enabled) {
		fieldList.setEnabled(enabled);
		addFieldButton.setEnabled(enabled);
		modifyFieldButton.setEnabled(enabled);
		deleteFieldButton.setEnabled(enabled);
		uniqueCheckBox.setEnabled(enabled);
		notNullCheckBox.setEnabled(enabled);
		foreignKeyNameTextField.setEnabled(enabled);
		foreignKeyRefFieldTextField.setEnabled(enabled);
		foreignKeyRefTableTextField.setEditable(enabled);
	}

	private void initializeComponents(Vector<Field> fields) {
		fieldListModel = new MyComboBoxModel<>(fields);
		fieldList = new JComboBox<>(fieldListModel);
		fieldList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (fieldListModel.getSelectedItem() == null) {
					return;
				}
				Field f = ((Field) fieldListModel.getSelectedItem());
				uniqueCheckBox.setSelected(f.isUnique());
				notNullCheckBox.setSelected(f.isNotNull());
				if (f.getForeignKey() != null) {
					foreignKeyNameTextField
							.setText(f.getForeignKey().getName());
					foreignKeyRefTableTextField.setText(f.getForeignKey()
							.getReferencedTable());
					foreignKeyRefFieldTextField.setText(f.getForeignKey()
							.getReferencedField());
				} else {
					foreignKeyNameTextField.setText("");
					foreignKeyRefTableTextField.setText("");
					foreignKeyRefFieldTextField.setText("");
				}
			}
		});

		addFieldButton = new JButton("Add");
		addFieldButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				final FieldEditor fieldEditor = new FieldEditor();
				final Field f = fieldEditor.runAdd();
				if (f != null) {
					fieldListModel.add(f);
				}
			}
		});

		modifyFieldButton = new JButton("Modify");
		modifyFieldButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				if (fieldList.getSelectedIndex() == -1) {
					return;
				}
				final FieldEditor fieldEditor = new FieldEditor();
				final Field f = fieldEditor.runModify((Field) fieldList
						.getSelectedItem());
				if (f != null) {
					fieldListModel.modify(f, fieldList.getSelectedIndex());
				}
			}
		});

		deleteFieldButton = new JButton("Delete");
		deleteFieldButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				if (fieldList.getSelectedIndex() == -1) {
					return;
				}
				fieldListModel.remove(fieldList.getSelectedIndex());
			}
		});

		uniqueCheckBox = new JCheckBox("Unique");
		uniqueCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (fieldListModel.getSelectedItem() == null) {
					return;
				}
				((Field) fieldListModel.getSelectedItem())
						.setUnique(uniqueCheckBox.isSelected());
			}
		});

		notNullCheckBox = new JCheckBox("Not null");
		notNullCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (fieldListModel.getSelectedItem() == null) {
					return;
				}
				((Field) fieldListModel.getSelectedItem())
						.setNotNull(notNullCheckBox.isSelected());
			}
		});

		foreignKeyNameTextField = new JTextField();
		foreignKeyNameTextField.addActionListener(new FkBuildAction());

		foreignKeyRefTableTextField = new JTextField();
		foreignKeyRefTableTextField.addActionListener(new FkBuildAction());

		foreignKeyRefFieldTextField = new JTextField();
		foreignKeyRefFieldTextField.addActionListener(new FkBuildAction());
	}

	private void prepareLayout() {
		panel.setLayout(new GridLayout(3, 1));
		panel.add(fieldList);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 5));
		buttonPanel.add(addFieldButton);
		buttonPanel.add(modifyFieldButton);
		buttonPanel.add(deleteFieldButton);
		buttonPanel.add(uniqueCheckBox);
		buttonPanel.add(notNullCheckBox);
		panel.add(buttonPanel);

		JPanel fkPanel = new JPanel();
		fkPanel.setLayout(new GridLayout(1, 6));
		fkPanel.add(new JLabel("FK name:"));
		fkPanel.add(foreignKeyNameTextField);
		fkPanel.add(new JLabel("FK table:"));
		fkPanel.add(foreignKeyRefTableTextField);
		fkPanel.add(new JLabel("FK field:"));
		fkPanel.add(foreignKeyRefFieldTextField);
		panel.add(fkPanel);
	}

	private ForeignKey buildForeignKey() {
		if (foreignKeyNameTextField.getText().equals("")
				&& foreignKeyRefTableTextField.getText().equals("")
				&& foreignKeyRefFieldTextField.getText().equals("")) {
			return null;
		}

		return new ForeignKey(foreignKeyNameTextField.getText(),
				foreignKeyRefTableTextField.getText(),
				foreignKeyRefFieldTextField.getText());
	}

	private class FkBuildAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (fieldListModel.getSelectedItem() == null) {
				return;
			}
			((Field) fieldListModel.getSelectedItem())
					.setForeignKey(buildForeignKey());
		}
	}
}
