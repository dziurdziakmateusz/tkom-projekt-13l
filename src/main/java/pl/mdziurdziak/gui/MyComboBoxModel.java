package pl.mdziurdziak.gui;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

class MyComboBoxModel<E> implements ComboBoxModel<E> {
	private Vector<E> objects;
	private List<ListDataListener> listeners = new LinkedList<>();
	private Object selected;

	public MyComboBoxModel(Vector<E> objects) {
		this.objects = objects;
	}

	@Override
	public int getSize() {
		return objects.size();
	}

	@Override
	public E getElementAt(int index) {
		return objects.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(l);
	}

	public void add(E toAdd) {
		objects.add(toAdd);
		for (ListDataListener l : listeners) {
			l.intervalAdded(new ListDataEvent(this,
					ListDataEvent.INTERVAL_ADDED, objects.size() - 1, objects
							.size() - 1));
		}
	}

	public void remove(int index) {
		E removed = objects.remove(index);
		if (removed == selected) {
			selected = null;
		}
		for (ListDataListener l : listeners) {
			l.intervalRemoved(new ListDataEvent(this,
					ListDataEvent.INTERVAL_REMOVED, index, index));
		}
	}

	public void modify(E field, int index) {
		objects.remove(index);
		objects.add(index, field);
		for (ListDataListener l : listeners) {
			l.contentsChanged(new ListDataEvent(this,
					ListDataEvent.CONTENTS_CHANGED, 0, objects.size() - 1));
		}
	}

	public E get(int index) {
		return objects.get(index);
	}

	@Override
	public void setSelectedItem(Object anItem) {
		selected = anItem;
	}

	@Override
	public Object getSelectedItem() {
		return selected;
	}
}
