package pl.mdziurdziak.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.sqlCreator.LogicException;
import pl.mdziurdziak.sqlCreator.MsSqlCreator;
import pl.mdziurdziak.sqlCreator.SqlCreator;

class EditorDialog extends JDialog implements TablePanelObserver {
	private static final long serialVersionUID = 1581469885178036768L;

	private JButton checkForErrorsButton;
	private JTextField pathTextField;
	private JButton choosePathButton;
	private JButton okButton;

	private TablePanel tablePanel;
	private FieldPanel fieldPanel;

	private Database database;

	public EditorDialog() {

	}

	public void run(Database database) {
		this.database = database;
		initializeComponents();
		prepareLayout();
		setTitle("Database editor");
		setModal(true);
		setSize(700, 400);
		setVisible(true);
	}

	@Override
	public void notifyObserver() {
		if (tablePanel.getSelectedTable() != null) {
			fieldPanel.setFields(tablePanel.getSelectedTable().getFields());
			fieldPanel.setEnabled(true);
		} else {
			fieldPanel.setFields(new Vector<Field>());
			fieldPanel.setEnabled(false);
		}
	}

	private void initializeComponents() {
		tablePanel = new TablePanel(database.getTables());
		tablePanel.addObserver(this);

		fieldPanel = new FieldPanel(new Vector<Field>());
		fieldPanel.setEnabled(false);

		checkForErrorsButton = new JButton("Check for errors");
		checkForErrorsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SqlCreator sqlCreator = new MsSqlCreator();
				try {
					sqlCreator.checkForErrors(database);
				} catch (LogicException e1) {
					JOptionPane.showMessageDialog(EditorDialog.this,
							"Logic exception\n" + e1.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				JOptionPane.showMessageDialog(EditorDialog.this, "Database ok",
						"Database checked", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		pathTextField = new JTextField();

		choosePathButton = new JButton("Choose output file");
		choosePathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser jFileChooser = new JFileChooser(
						new File("."));
				jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				jFileChooser.setMultiSelectionEnabled(false);
				int returnVal = jFileChooser.showOpenDialog(EditorDialog.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					pathTextField.setText(jFileChooser.getSelectedFile()
							.getAbsolutePath());
				}
			}
		});

		okButton = new JButton("Ok");
		okButton.addActionListener(new OkAction());
	}

	private void prepareLayout() {
		JPanel jp = new JPanel();
		jp.setBorder(BorderFactory.createTitledBorder("Database"));
		jp.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridwidth = 2;
		tablePanel.setBorder(BorderFactory.createTitledBorder("Tables"));
		jp.add(tablePanel, gbc);

		gbc.gridy = 1;
		fieldPanel.setBorder(BorderFactory.createTitledBorder("Fields"));
		jp.add(fieldPanel, gbc);

		gbc.gridy = 2;
		gbc.gridwidth = 1;
		jp.add(pathTextField, gbc);

		gbc.gridx = 1;
		jp.add(choosePathButton, gbc);

		gbc.gridy = 3;
		gbc.gridx = 0;
		jp.add(checkForErrorsButton, gbc);

		gbc.gridx = 1;
		jp.add(okButton, gbc);

		add(jp);
	}

	private class OkAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			final File outputFile = new File(pathTextField.getText());
			if (!outputFile.exists()) {
				JOptionPane.showMessageDialog(EditorDialog.this,
						"File doesn't exists.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(outputFile);
				final SqlCreator sqlCreator = new MsSqlCreator();
				sqlCreator.write(fos, database);
				fos.close();
				JOptionPane.showMessageDialog(EditorDialog.this,
						"SQL successfully created.", "Success",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(EditorDialog.this,
						"File not found", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			} catch (LogicException e) {
				JOptionPane.showMessageDialog(EditorDialog.this,
						"Logic exception\n" + e.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				try {
					fos.close();
				} catch (IOException e1) {
				}
			} catch (IOException e) {
				JOptionPane.showMessageDialog(EditorDialog.this,
						"IO exception\n" + e.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
	}
}
