package pl.mdziurdziak.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import pl.mdziurdziak.databaseStructure.Table;

class TableEditor extends JDialog {
	private static final long serialVersionUID = -890979096278298598L;

	private JTextField tableNameTextField;
	private JButton okButton;
	private JButton cancelButton;

	private boolean ok = false;

	public TableEditor() {
		tableNameTextField = new JTextField();

		okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tableNameTextField.getText().equals("")) {
					JOptionPane.showMessageDialog(TableEditor.this,
							"Name cannot be empty", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				ok = true;
				TableEditor.this.setVisible(false);
			}
		});

		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ok = false;
				TableEditor.this.setVisible(false);
			}
		});

		setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.BOTH;
		add(tableNameTextField, gbc);

		gbc.gridy = 1;
		gbc.weightx = 1;
		gbc.gridwidth = 1;
		add(okButton, gbc);

		gbc.gridx = 1;
		add(cancelButton, gbc);

		setModal(true);
		setTitle("Insert table name");
		setSize(400, 100);
	}

	public Table runAdd() {
		ok = false;
		tableNameTextField.setText("");
		setVisible(true);
		if (ok && !tableNameTextField.getText().equals("")) {
			return new Table(tableNameTextField.getText());
		}
		return null;
	}

	public Table runModify(Table table) {
		ok = false;
		tableNameTextField.setText(table.getName());
		setVisible(true);
		if (ok && !tableNameTextField.getText().equals("")) {
			return new Table(tableNameTextField.getText());
		}
		return null;
	}
}
