package pl.mdziurdziak.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class StringGetter extends JDialog {
	private static final long serialVersionUID = -2036899046790300701L;

	private JTextField toRet;
	private JButton okButton;
	private JButton cancelButton;
	
	private boolean ok = false;

	public StringGetter() {
		setSize(100, 40);
		
		toRet = new JTextField();
		
		okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (toRet.getText().equals("")) {
					JOptionPane.showMessageDialog(StringGetter.this,
							"Name cannot be empty", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				ok = true;
				StringGetter.this.setVisible(false);
			}
		});
		
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ok = false;
				StringGetter.this.setVisible(false);
			}
		});
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.BOTH;
		add(toRet, gbc);

		gbc.gridy = 1;
		gbc.weightx = 1;
		gbc.gridwidth = 1;
		add(okButton, gbc);

		gbc.gridx = 1;
		add(cancelButton, gbc);

		setModal(true);
		setSize(400, 100);
	}

	public StringGetter(String string) {
		this();
		setTitle(string);
	}

	public String getString() {
		ok = false;
		toRet.setText("");
		setVisible(true);
		if(ok && !toRet.getText().equals("")){
			return toRet.getText();
		}
		return null;
	}
}
