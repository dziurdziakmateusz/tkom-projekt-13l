package pl.mdziurdziak.gui;

interface TablePanelObserver {
	public void notifyObserver();
}
