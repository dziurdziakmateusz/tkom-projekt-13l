package pl.mdziurdziak.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import pl.mdziurdziak.databaseStructure.Field;

public class FieldEditor extends JDialog {
	private static final long serialVersionUID = 2683673816747753728L;

	private JTextField fieldNameTextField;
	private JTextField fieldTypeTextField;
	private JButton okButton;
	private JButton cancelButton;

	private boolean ok = false;

	public FieldEditor() {
		initializeComponents();

		setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.BOTH;
		add(new JLabel("Name"), gbc);

		gbc.gridx = 1;
		add(fieldNameTextField, gbc);

		gbc.gridy = 1;
		gbc.gridx = 0;
		add(new JLabel("Type"), gbc);

		gbc.gridx = 1;
		add(fieldTypeTextField, gbc);

		gbc.gridy = 2;
		gbc.gridx = 0;
		add(okButton, gbc);

		gbc.gridx = 1;
		add(cancelButton, gbc);

		setModal(true);
		setTitle("Insert basic field data");
		setSize(400, 200);
	}

	private void initializeComponents() {
		fieldNameTextField = new JTextField();

		fieldTypeTextField = new JTextField();

		okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (fieldNameTextField.getText().equals("")) {
					JOptionPane.showMessageDialog(FieldEditor.this,
							"Name cannot be empty", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				if (fieldTypeTextField.getText().equals("")) {
					JOptionPane.showMessageDialog(FieldEditor.this,
							"Type cannot be empty", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ok = true;
				FieldEditor.this.setVisible(false);
			}
		});

		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ok = false;
				FieldEditor.this.setVisible(false);
			}
		});
	}

	public Field runAdd() {
		ok = false;
		fieldNameTextField.setText("");
		fieldNameTextField.setText("");
		setVisible(true);
		if (ok && !fieldNameTextField.getText().equals("")
				&& !fieldTypeTextField.equals("")) {
			return new Field(fieldNameTextField.getText(),
					fieldTypeTextField.getText());
		}
		return null;
	}

	public Field runModify(Field field) {
		ok = false;
		fieldNameTextField.setText(field.getName());
		fieldTypeTextField.setText(field.getType());
		setVisible(true);
		if (ok && !fieldNameTextField.getText().equals("")
				&& !fieldTypeTextField.equals("")) {
			return new Field(fieldNameTextField.getText(),
					fieldTypeTextField.getText());
		}
		return null;
	}

}
