package pl.mdziurdziak.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import pl.mdziurdziak.compiler.CompilerException;
import pl.mdziurdziak.compiler.CompilerImpl;
import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.parser.Node;
import pl.mdziurdziak.parser.Parser;
import pl.mdziurdziak.parser.ParserImpl;
import pl.mdziurdziak.tokenizer.Tokenizer;
import pl.mdziurdziak.tokenizer.TokenizerImpl;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = -3592601151326394030L;

	private JTextField pathTextField;
	private JButton choosePathButton;
	private JButton okButton;
	private MenuBar menuBar;

	public MainFrame() {
		initializeComponents();
		prepareLayout();
		setSize(400, 120);
	}

	private void initializeComponents() {
		pathTextField = new JTextField();

		choosePathButton = new JButton("Choose file");
		choosePathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser jFileChooser = new JFileChooser(
						new File("."));
				jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				jFileChooser.setMultiSelectionEnabled(false);
				int returnVal = jFileChooser.showOpenDialog(MainFrame.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					pathTextField.setText(jFileChooser.getSelectedFile()
							.getAbsolutePath());
				}
			}
		});

		okButton = new JButton("Ok");
		okButton.addActionListener(new OkAction());
		
		menuBar = new MenuBar();
		Menu infoMenu = new Menu("Info");
		MenuItem menuItem = new MenuItem("About");
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane
						.showMessageDialog(
								MainFrame.this,
								"<html><b>Author</b>: <i>Dziurdziak Mateusz</i><br><br><b>"
										+ "Project Name:</b> <i>XML to SQL Conventer</i><br><br>"
										+ "<i>2013-04-28</i><html>",
								"About project",
								JOptionPane.INFORMATION_MESSAGE);
			}
		});
		infoMenu.add(menuItem);
		menuBar.add(infoMenu);
	}

	private void prepareLayout() {
		setLayout(new GridBagLayout());

		final GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		add(pathTextField, gbc);

		final GridBagConstraints cpbGbc = new GridBagConstraints();
		cpbGbc.gridx = 0;
		cpbGbc.gridy = 1;
		cpbGbc.gridheight = 1;
		cpbGbc.gridwidth = 1;
		cpbGbc.fill = GridBagConstraints.BOTH;
		cpbGbc.weightx = 1;
		cpbGbc.weighty = 1;
		add(choosePathButton, cpbGbc);

		final GridBagConstraints okGbc = new GridBagConstraints();
		okGbc.gridx = 1;
		okGbc.gridy = 1;
		okGbc.gridheight = 1;
		okGbc.gridwidth = 1;
		okGbc.fill = GridBagConstraints.BOTH;
		okGbc.weightx = 1;
		okGbc.weighty = 1;
		add(okButton, okGbc);

		setMenuBar(menuBar);
	}
	
	private class OkAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			final File sourceFile = new File(pathTextField.getText());
			if (!sourceFile.exists()) {
				JOptionPane.showMessageDialog(MainFrame.this,
						"File doesn't exists.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(sourceFile);
				final Tokenizer tokenizer = new TokenizerImpl();
				tokenizer.setInputStream(fis);
				final Parser parser = new ParserImpl();
				final Node documentNode = parser.parse(tokenizer);
				fis.close();
				pl.mdziurdziak.compiler.Compiler compiler = new CompilerImpl();
				final Database database = compiler.compile(documentNode);
				EditorDialog editorDialog = new EditorDialog();
				editorDialog.run(database);
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(MainFrame.this,
						"File not found", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			} catch (ParseException e1) {
				JOptionPane.showMessageDialog(MainFrame.this,
						"Parse exception\n"+e1.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				try {
					fis.close();
				} catch (IOException e2) {
				}
				return;
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(MainFrame.this,
						"IO exception\n"+e1.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			} catch (CompilerException e1) {
				JOptionPane.showMessageDialog(MainFrame.this,
						"Compiler exception\n"+e1.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
	}
}
