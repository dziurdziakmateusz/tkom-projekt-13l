package pl.mdziurdziak.tokenizer;

/**
 * Token zwracany przez Tokenzier. Zawiera typ oraz wartość.
 * 
 * @see Tokenizer
 * @see TokenIdentifier
 * @author Mateusz Dziurdziak
 */
public class Token {
	private TokenIdentifier id;
	private String value;

	public Token() {

	}

	public Token(TokenIdentifier id, String value) {
		this.id = id;
		this.value = value;
	}

	public TokenIdentifier getId() {
		return id;
	}

	public void setId(TokenIdentifier id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
