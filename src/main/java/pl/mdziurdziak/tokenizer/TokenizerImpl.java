package pl.mdziurdziak.tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Preconditions.checkNotNull;

import static pl.mdziurdziak.tokenizer.TokenIdentifier.*;

/**
 * @author Mateusz Dziurdziak
 */
public class TokenizerImpl implements Tokenizer {

	private static final char[] COMMENT_BEGIN_SIGNS = { '-', '-' };
	private static final char[] CLOSE_SIGNS = { '/' };
	private static final char[] ONE_LINE_CLOSE_SIGNS = { '>' };
	private static final char[] COMMENT_END_SIGNS = { '-', '>' };
	private static final char[] LT_STRING_SIGNS = { 'l', 't' };
	private static final char[] GT_STRING_SIGNS = { 'g', 't' };
	private static final char[] AMP_STRING_SIGNS = { 'a', 'm', 'p' };
	private static final char[] QUOTATION_STRING_SIGNS = { 'q', 'u', 'o', 't' };
	private static final char[] APOS_STRING_SIGNS = { 'a', 'p', 'o', 's' };
	private static final char[] CHARACTERS = { '!', '@', '#', '%', '^', '&',
			'*', '(', ')', '{', '}', '[', ']', ';', ':', ',', '.', '?', '/',
			'\\', '-', };

	private InputStream is;
	private Reader reader;
	private List<Integer> buffer;

	@Override
	public void setInputStream(InputStream is) {
		checkNotNull(is, "You cannont set null input stream");
		this.is = is;
		reader = new BufferedReader(new InputStreamReader(is));
		buffer = new LinkedList<>();
	}

	public InputStream getInputStream() {
		return is;
	}

	@Override
	public Token getNextToken() throws IOException {
		checkState(is != null, "You cannot get token from null input stream.");

		final Token token = new Token();
		char ch;

		for (;;) {
			int intReaded = getNext();
			if (intReaded == -1) {
				token.setId(EOF);
				return token;
			}

			ch = (char) intReaded;
			if (ch == '\n' || ch == '\t' || ch=='\r')
				continue;
			else
				break;
		}

		token.setValue("" + ch);
		if (ch == '<') {
			if (!tryReadCommentBegin(token) && !tryReadClose(token))
				token.setId(LT);
		} else if (ch == '>') {
			token.setId(GT);
		} else if (ch == '/') {
			if (!tryReadOneLineClose(token))
				token.setId(CHARACTER);
		} else if (ch == '-') {
			if (!tryReadCommentEnd(token))
				token.setId(CHARACTER);
		} else if (ch == '=') {
			token.setId(EQUALS);
		} else if (ch == '"') {
			token.setId(QUOTATION);
		} else if (ch == ' ') {
			token.setId(SPACE);
		} else if (ch == '&') {
			if (!tryReadLtString(token) && !tryReadGtString(token)
					&& !tryReadAmpString(token)
					&& !tryReadQuotationString(token)
					&& !tryReadAposString(token))
				token.setId(CHARACTER);
		} else if (isCharacter(ch)) {
			token.setId(CHARACTER);
		} else if (isAllowedInSimpleString(ch)) {
			readSimpleString(token);
		}

		return token;
	}

	private boolean isAllowedInSimpleString(char ch) {
		if (ch == '_' || Character.isDigit(ch) || Character.isLetter(ch))
			return true;

		return false;
	}

	private void readSimpleString(Token token) throws IOException {
		token.setId(SIMPLE_STRING);
		for (;;) {
			int next = getNext();

			if (next == -1)
				return;

			if (!isAllowedInSimpleString((char) next)) {
				buffer.add(0, next);
				return;
			} else {
				token.setValue(token.getValue() + (char) next);
			}
		}
	}

	private boolean isCharacter(char ch) {
		for (int i = 0; i < CHARACTERS.length; i++)
			if (ch == CHARACTERS[i])
				return true;

		return false;
	}

	private boolean tryReadAposString(Token token) throws IOException {
		if (tryRead(APOS_STRING_SIGNS)) {
			token.setId(APOS_STRING);
			token.setValue("&apos");
			return true;
		}
		return false;
	}

	private boolean tryReadQuotationString(Token token) throws IOException {
		if (tryRead(QUOTATION_STRING_SIGNS)) {
			token.setId(QUOTATION_STRING);
			token.setValue("&quot");
			return true;
		}
		return false;
	}

	private boolean tryReadAmpString(Token token) throws IOException {
		if (tryRead(AMP_STRING_SIGNS)) {
			token.setId(AMP_STRING);
			token.setValue("&amp");
			return true;
		}
		return false;
	}

	private boolean tryReadGtString(Token token) throws IOException {
		if (tryRead(GT_STRING_SIGNS)) {
			token.setId(GT_STRING);
			token.setValue("&gt");
			return true;
		}
		return false;
	}

	private boolean tryReadLtString(Token token) throws IOException {
		if (tryRead(LT_STRING_SIGNS)) {
			token.setId(LT_STRING);
			token.setValue("&lt");
			return true;
		}
		return false;
	}

	private boolean tryReadCommentEnd(Token token) throws IOException {
		if (tryRead(COMMENT_END_SIGNS)) {
			token.setId(COMMENT_END);
			token.setValue("-->");
			return true;
		}
		return false;
	}

	private boolean tryReadOneLineClose(Token token) throws IOException {
		if (tryRead(ONE_LINE_CLOSE_SIGNS)) {
			token.setId(ONE_LINE_CLOSE);
			token.setValue("/>");
			return true;
		}
		return false;
	}

	private boolean tryReadClose(Token token) throws IOException {
		if (tryRead(CLOSE_SIGNS)) {
			token.setId(CLOSE);
			token.setValue("</");
			return true;
		}
		return false;
	}

	private boolean tryReadCommentBegin(Token token) throws IOException {
		if (tryRead(COMMENT_BEGIN_SIGNS)) {
			token.setId(COMMENT_BEGIN);
			token.setValue("<--");
			return true;
		}
		return false;
	}

	/**
	 * Dopasowuje następne znaki ze strumienia do podanych. Jeśli są zgodne
	 * zwraca true. Jeśli nie odczytane znaki umieszcza w buforze, oraz zwraca
	 * false.
	 * 
	 * @throws IOException
	 */
	private boolean tryRead(char[] signs) throws IOException {
		for (int i = 0; i < signs.length; i++) {
			int ch = getNext();
			if (ch == -1 || ((char) ch != signs[i])) {
				{
					buffer.add(0, ch);

					for (int j = i - 1; j >= 0; j--)
						buffer.add(0, (int) signs[j]);

					return false;
				}
			}
		}
		return true;
	}

	private int getNext() throws IOException {
		if (buffer.size() > 0) {
			return buffer.remove(0);
		}

		return reader.read();
	}
}
