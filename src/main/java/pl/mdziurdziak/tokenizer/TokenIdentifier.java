package pl.mdziurdziak.tokenizer;

/**
 * Enum identyfikujący możliwe typy tokenów.
 * 
 * @see Token
 * @author Mateusz Dziurdziak
 */
public enum TokenIdentifier {
	COMMENT_BEGIN, // <--
	CLOSE, // </
	LT, // <
	GT, // >
	ONE_LINE_CLOSE, // />
	COMMENT_END, // -->
	SIMPLE_STRING, EQUALS, // =
	QUOTATION, // "
	SPACE, LT_STRING, // &lt
	GT_STRING, // &gt
	AMP_STRING, // &amp
	QUOTATION_STRING, // &quot
	APOS_STRING, // &apos
	CHARACTER, // ! @ # % ^ & * ( ) { } [ ] ; : , . ? / \ -
	EOF
}
