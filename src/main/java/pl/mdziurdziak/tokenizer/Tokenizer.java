package pl.mdziurdziak.tokenizer;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interfejs Tokenizer musi implementować każda klasa aspirujaca do parsowania
 * dokumentu w celu wytwarzania tokenów.
 * 
 * @see TokenizerImpl
 * @author Mateusz Dziurdziak
 */
public interface Tokenizer {
	public void setInputStream(InputStream is);

	public Token getNextToken() throws IOException;
}
