package pl.mdziurdziak;

import javax.swing.JFrame;

import pl.mdziurdziak.gui.MainFrame;

public class Main {
	private static final String APPLICATION_TITLE = "XML -> SQL Conventer";

	public static void main(String[] args) {
		final JFrame frame = new MainFrame();
		frame.setTitle(APPLICATION_TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}

}


