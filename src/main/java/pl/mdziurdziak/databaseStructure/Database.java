package pl.mdziurdziak.databaseStructure;

import java.util.Vector;

/**
 * Klasa reprezentująca strukturę bazy danych.
 * 
 * @see Table
 * @author Mateusz Dziurdziak
 */
public class Database {
	private String name;
	private Vector<Table> tables;

	public Database(String name) {
		this.name = name;
		tables = new Vector<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<Table> getTables() {
		return tables;
	}

	public void setTables(Vector<Table> tables) {
		this.tables = tables;
	}

	public Table getTableByName(String name) {
		for (Table table : tables) {
			if (table.getName().equalsIgnoreCase(name)) {
				return table;
			}
		}

		return null;
	}
	
	@Override
	public String toString() {
		return name + " " + tables.toString();
	}
}
