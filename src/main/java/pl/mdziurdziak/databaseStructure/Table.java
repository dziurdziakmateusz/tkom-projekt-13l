package pl.mdziurdziak.databaseStructure;

import java.util.Vector;

/**
 * Klasa reprezentująca tabelę bazy danych.
 * 
 * @see Field
 * @see PrimaryKey
 * @author Mateusz Dziurdziak
 */
public class Table {
	private String name;
	private Vector<Field> fields;
	private PrimaryKey primaryKey;

	public Table(String name) {
		this.name = name;
		fields = new Vector<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<Field> getFields() {
		return fields;
	}

	public void setFields(Vector<Field> fields) {
		this.fields = fields;
	}

	public PrimaryKey getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(PrimaryKey primaryKey) {
		this.primaryKey = primaryKey;
	}

	public void addField(Field field) {
		fields.add(field);
	}

	public Field getFieldByName(String name) {
		for (Field field : fields) {
			if (field.getName().equalsIgnoreCase(name)) {
				return field;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
