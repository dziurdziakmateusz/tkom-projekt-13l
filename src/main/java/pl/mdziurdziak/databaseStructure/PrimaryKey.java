package pl.mdziurdziak.databaseStructure;

import java.util.Vector;

/**
 * Klasa reprezentująca klucz główny tabeli bazy danych.
 * 
 * @see Table
 * @author Mateusz Dziurdziak
 */
public class PrimaryKey {
	private String name;
	private Vector<String> fields;

	public PrimaryKey(String name) {
		this.name = name;
		fields = new Vector<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<String> getFields() {
		return fields;
	}

	public void setFields(Vector<String> fields) {
		this.fields = fields;
	}

	public void addField(String field) {
		fields.add(field);
	}

	@Override
	public String toString() {
		return name + " " + fields.toString();
	}
}
