package pl.mdziurdziak.databaseStructure;

/**
 * Klasa reprezentująca klucz obcy w bazie danych.
 * 
 * @see Field
 * @author Mateusz Dziurdziak
 */
public class ForeignKey {
	private String name;
	private String referencedTable;
	private String referencedField;

	public ForeignKey(String name, String referencedTable,
			String referencedField) {
		this.name = name;
		this.referencedTable = referencedTable;
		this.referencedField = referencedField;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReferencedTable() {
		return referencedTable;
	}

	public void setReferencedTable(String referencedTable) {
		this.referencedTable = referencedTable;
	}

	public String getReferencedField() {
		return referencedField;
	}

	public void setReferencedField(String referencedField) {
		this.referencedField = referencedField;
	}

	@Override
	public String toString() {
		return "FK NAME:"+name+" RefTab:"+referencedTable+" RefField:"+referencedField;
	}
}
