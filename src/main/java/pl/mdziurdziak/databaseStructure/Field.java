package pl.mdziurdziak.databaseStructure;

/**
 * Klasa reprezentująca pole tabeli.
 * 
 * @see Table
 * @see ForeignKey
 * @author Mateusz Dziurdziak
 */
public class Field {
	private String name;
	private String type;
	private boolean unique;
	private boolean notNull;
	private ForeignKey foreignKey;

	public Field(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}

	public ForeignKey getForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(ForeignKey foreignKey) {
		this.foreignKey = foreignKey;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
