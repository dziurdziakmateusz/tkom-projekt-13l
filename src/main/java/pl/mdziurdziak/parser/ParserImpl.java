package pl.mdziurdziak.parser;

import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

import pl.mdziurdziak.tokenizer.Token;
import pl.mdziurdziak.tokenizer.TokenIdentifier;
import pl.mdziurdziak.tokenizer.Tokenizer;

import static pl.mdziurdziak.tokenizer.TokenIdentifier.*;

/**
 * @author Mateusz Dziurdziak
 */
public class ParserImpl implements Parser {

	private List<Token> buffer = new LinkedList<>();

	@Override
	public Node parse(Tokenizer tokenizer) throws ParseException, IOException {
		checkNotNull(tokenizer, "Tokenizer cannot be null");
		final Node documentNode = read(tokenizer);
		return documentNode;
	}

	private Node read(Tokenizer tokenizer) throws ParseException, IOException {
		while (true) {
			Token token = getNextToken(tokenizer);
			if (token.getId() == SPACE)
				continue;

			if (token.getId() == EOF)
				return null;

			if (token.getId() == COMMENT_BEGIN) {
				readComment(tokenizer);
				continue;
			}

			if (token.getId() != LT)
				throw new ParseException("Readed " + token.getId()
						+ ", expected: " + LT, 0);

			Node node = readNode(tokenizer);
			return node;
		}

	}

	private Node readNode(Tokenizer tokenizer) throws IOException,
			ParseException {
		final Token nodeName = getNextToken(tokenizer);
		if (nodeName.getId() != SIMPLE_STRING)
			throw new ParseException("Readed " + nodeName.getId()
					+ ", expected: " + SIMPLE_STRING, 0);

		final Node node = new Node(nodeName.getValue());

		Token nextToken = skipSpaces(tokenizer);

		if (nextToken.getId() == SIMPLE_STRING) {
			while (true) {
				Attribute attribute = readAttribute(tokenizer,
						nextToken.getValue());
				node.addAttribute(attribute);
				nextToken = skipSpaces(tokenizer);
				if (nextToken.getId() == SIMPLE_STRING)
					continue;
				else
					break;
			}
		}

		if (nextToken.getId() == ONE_LINE_CLOSE) {
			return node;
		} else if (nextToken.getId() == GT) {
			readNodeBody(tokenizer, node);
			nextToken = getNextToken(tokenizer);
			if (nextToken.getId() != SIMPLE_STRING)
				throw new ParseException(
						"Unexpected token while reading node end", 0);
			if (!nextToken.getValue().equals(node.getName()))
				throw new ParseException("", 0);
			nextToken = skipSpaces(tokenizer);
			if (nextToken.getId() != GT)
				throw new ParseException("Not properly ended element", 0);
		} else {
			throw new ParseException("Readed " + nextToken.getId()
					+ ", expected element end", 0);
		}

		return node;
	}

	private void readNodeBody(Tokenizer tokenizer, Node node)
			throws IOException, ParseException {
		for (;;) {
			Token token = skipSpaces(tokenizer);

			if (token.getId() == LT) {
				Node child = readNode(tokenizer);
				node.addChild(child);
			} else if (token.getId() == CLOSE) {
				return;
			} else if(token.getId() == COMMENT_BEGIN) {
				readComment(tokenizer);
			} else {
				throw new ParseException(
						"Unexpected token while reading node body", 0);
			}
		}
	}

	private Attribute readAttribute(Tokenizer tokenizer, String attributeName)
			throws IOException, ParseException {
		Attribute attribute = new Attribute(attributeName, "");
		Token equals = skipSpaces(tokenizer);
		if (equals.getId() != EQUALS)
			throw new ParseException("Readed " + equals.getId()
					+ ", expected: " + EQUALS, 0);
		Token quot = skipSpaces(tokenizer);
		if (quot.getId() != QUOTATION)
			throw new ParseException("Readed " + quot.getId() + ", expected: "
					+ QUOTATION, 0);

		String value = readComplexString(tokenizer);

		attribute.setValue(value);
		return attribute;
	}

	private String readComplexString(Tokenizer tokenizer) throws IOException,
			ParseException {
		StringBuilder sb = new StringBuilder();
		Token token;
		for (;;) {
			token = getNextToken(tokenizer);
			if (token.getId() == SPACE || token.getId() == CHARACTER
					|| token.getId() == SIMPLE_STRING) {
				sb.append(token.getValue());
			} else if (token.getId() == LT_STRING || token.getId() == GT_STRING
					|| token.getId() == AMP_STRING
					|| token.getId() == QUOTATION_STRING
					|| token.getId() == APOS_STRING) {
				sb.append(getStringValue(token.getId(), token.getValue()));
			} else if (token.getId() == QUOTATION) {
				break;
			} else {
				throw new ParseException("Unexpected token (" + token.getId()
						+ ") while reading complex string", 0);
			}
		}
		return sb.toString();
	}

	private String getStringValue(TokenIdentifier id, String value) {
		switch (id) {
		case LT_STRING:
			return "<";
		case GT_STRING:
			return ">";
		case AMP_STRING:
			return "&";
		case QUOTATION_STRING:
			return "\"";
		case APOS_STRING:
			return "'";
		default:
			throw new IllegalArgumentException("");
		}
	}

	private Token skipSpaces(Tokenizer tokenizer) throws IOException,
			ParseException {
		Token token;
		while ((token = getNextToken(tokenizer)).getId() == SPACE) {
			;
		}

		return token;
	}

	private void readComment(Tokenizer tokenizer) throws IOException,
			ParseException {
		Token token;
		while (true) {
			token = getNextToken(tokenizer);
			if (token.getId() == EOF)
				throw new ParseException("End of file while reading a comment",
						0);

			if (token.getId() == COMMENT_END)
				return;
		}

	}

	private Token getNextToken(Tokenizer tokenizer) throws IOException {
		if (buffer.size() > 0)
			return buffer.remove(0);

		return tokenizer.getNextToken();
	}
}
