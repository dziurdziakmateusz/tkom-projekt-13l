package pl.mdziurdziak.parser;

import java.io.IOException;
import java.text.ParseException;

import pl.mdziurdziak.tokenizer.Tokenizer;

/**
 * Klasa Parser buduje drzewo dokumentu xml wykorzystując do tego tokeny
 * podawane przez Tokenizer.
 * 
 * @see ParserImpl
 * @author Mateusz Dziurdziak
 */
public interface Parser {
	public Node parse(Tokenizer tokenizer) throws ParseException, IOException;
}
