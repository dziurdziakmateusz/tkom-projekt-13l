package pl.mdziurdziak.parser;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Mateusz Dziurdziak
 */
public class Node {
	private String name;
	private List<Attribute> attributes;
	private List<Node> children;

	public Node(String name) {
		this.name = name;
		attributes = new LinkedList<>();
		children = new LinkedList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Zwraca kolekcję atrybutów (nie do modyfikacji).
	 */
	public List<Attribute> getAttributes() {
		return Collections.unmodifiableList(attributes);
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return Zwraca listę dzieci (nie do modyfikacji).
	 */
	public List<Node> getChildren() {
		return Collections.unmodifiableList(children);
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public void addAttribute(Attribute attribute) {
		attributes.add(attribute);
	}

	public void addChild(Node child) {
		children.add(child);
	}

	public Attribute getAttributeByName(String name) {
		for (Attribute attribute : attributes) {
			if (attribute.getName().equalsIgnoreCase(name)) {
				return attribute;
			}
		}

		return null;
	}

	public Node getChildByName(String name) {
		for (Node node : children) {
			if (node.getName().equalsIgnoreCase(name)) {
				return node;
			}
		}

		return null;
	}

	public List<Attribute> listAttributesByName(String name) {
		List<Attribute> toReturn = new LinkedList<>();

		for (Attribute attribute : attributes) {
			if (attribute.getName().equalsIgnoreCase(name)) {
				toReturn.add(attribute);
			}
		}

		return toReturn;
	}

	public List<Node> listChildrenByName(String name) {
		List<Node> toReturn = new LinkedList<>();

		for (Node node : children) {
			if (node.getName().equalsIgnoreCase(name)) {
				toReturn.add(node);
			}
		}

		return toReturn;
	}
}
