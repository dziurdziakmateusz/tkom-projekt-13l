package pl.mdziurdziak.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.databaseStructure.ForeignKey;
import pl.mdziurdziak.databaseStructure.PrimaryKey;
import pl.mdziurdziak.databaseStructure.Table;
import pl.mdziurdziak.parser.Attribute;
import pl.mdziurdziak.parser.Node;

/**
 * @author Mateusz Dziurdziak
 */
public class CompilerImpl implements Compiler {

	private Map<String, FieldMask> fieldMasks;

	@Override
	public Database compile(Node schemaNode) throws CompilerException {
		checkNotNull(schemaNode, "Node cannot be null");
		if (!schemaNode.getName().equalsIgnoreCase("schema")) {
			throw new CompilerException("Main node has incorrect name.");
		}
		checkSchemaChildrenNames(schemaNode);
		readFieldMasks(schemaNode);

		return readDatabase(schemaNode);
	}

	/**
	 * Sprawdza czy dzieci głównego węzła mają właściwe nazwy (dozwolone
	 * <i>fieldMask</i> oraz <i>table</i>) oraz czy krotność węzła
	 * <i>database</i> wynosi 1.
	 * 
	 * @param schemaNode
	 *            Główny Node sparsowanego dokumentu
	 * @throws CompilerException
	 *             Rzucany w przypadku natrafienia na niewłaściwą nazwę
	 */
	private void checkSchemaChildrenNames(Node schemaNode)
			throws CompilerException {
		for (Node child : schemaNode.getChildren()) {
			if (!child.getName().equalsIgnoreCase("fieldMask")
					&& child.getName().equalsIgnoreCase("table")) {
				throw new CompilerException("Incorrect child node in main node");
			}
		}
	}

	/**
	 * Odczytuje maski pól z głownego Node dokumentu oraz zapisuje je w
	 * fieldMasks.
	 * 
	 * @param schemaNode
	 *            Główny Node sparsowanego dokumentu.
	 * @throws CompilerException
	 *             Rzucany gdy struktura węzła fieldMask będzie niepoprawna.
	 */
	private void readFieldMasks(Node schemaNode) throws CompilerException {
		fieldMasks = new HashMap<>();
		for (Node node : schemaNode.listChildrenByName("fieldMask")) {
			if (node.getChildren().size() != 0) {
				throw new CompilerException(
						"Incorrect children nodes count in fieldMask.");
			}

			final Attribute id = node.getAttributeByName("id");
			final Attribute mask = node.getAttributeByName("masK");

			if (id == null || mask == null || node.getAttributes().size() != 2) {
				throw new CompilerException("Incorrect fieldMask attributes.");
			}

			final FieldMask fm = new FieldMask();
			fm.setId(id.getValue());
			fm.setMask(mask.getValue());

			fieldMasks.put(fm.getId(), fm);
		}
	}

	/**
	 * Odczytuje schemat bazy danych z węzła głownego dokumentu.
	 * 
	 * @param schemaNode
	 *            Główny Node sparsowanego dokumentu
	 * @return Odczytana baza danych.
	 * @throws CompilerException
	 *             Rzucany gdy krotność <i>database</i> jest niewłaściwa lub
	 *             wystąpił problem ze struturą węzłów.
	 */
	private Database readDatabase(Node schemaNode) throws CompilerException {
		if (schemaNode.listChildrenByName("database").size() != 1) {
			throw new CompilerException(
					"Incorrect <i>database</i> nodes count.");
		}

		return readDatabaseNode(schemaNode.getChildByName("database"));
	}

	private Database readDatabaseNode(Node databaseNode)
			throws CompilerException {
		final Database database = new Database(readDatabaseName(databaseNode));

		for (Node child : databaseNode.getChildren()) {
			if (child.getName().equalsIgnoreCase("table")) {
				final Table table = readTable(child);
				database.getTables().add(table);
			} else {
				throw new CompilerException(
						"Incorrect children nodes in database node");
			}
		}
		return database;
	}

	private Table readTable(Node tableNode) throws CompilerException {
		final Table table = new Table(readTableName(tableNode));

		final List<Node> fieldsNodes = tableNode.listChildrenByName("field");
		final List<Node> pkNodes = tableNode.listChildrenByName("pk");

		if (pkNodes.size() > 1) {
			throw new CompilerException("Too many pk for table "
					+ table.getName());
		}

		if (fieldsNodes.size() + pkNodes.size() != tableNode.getChildren()
				.size()) {
			throw new CompilerException(
					"Incorrect children nodes in table node");
		}

		final Vector<Field> fields = readFields(fieldsNodes);
		table.setFields(fields);

		if (pkNodes.size() == 1) {
			final PrimaryKey pk = readPrimaryKey(pkNodes.get(0));
			table.setPrimaryKey(pk);
		}

		return table;
	}

	private Vector<Field> readFields(List<Node> fieldsNodes)
			throws CompilerException {
		final Vector<Field> fields = new Vector<>();
		for (Node node : fieldsNodes) {
			fields.add(readField(node));
		}
		return fields;
	}

	private PrimaryKey readPrimaryKey(Node pkNode) throws CompilerException {
		if (pkNode.getChildren().size() > 0) {
			final List<Attribute> names = pkNode.listAttributesByName("name");
			final List<Node> pkFieldsNames = pkNode
					.listChildrenByName("pkField");
			if (names.size() > 1
					|| pkNode.getAttributes().size() != names.size()
					|| pkFieldsNames.size() == 0
					|| pkFieldsNames.size() != pkNode.getChildren().size()) {
				throw new CompilerException("");
			}

			final PrimaryKey pk = new PrimaryKey(names.size() == 0 ? "" : names
					.get(0).getValue());

			for (Node node : pkFieldsNames) {
				final List<Attribute> fn = node
						.listAttributesByName("fieldName");
				if (fn.size() != 1 || node.getAttributes().size() != fn.size()
						|| node.getChildren().size() != 0) {
					throw new CompilerException("");
				}

				pk.addField(fn.get(0).getValue());
			}

			return pk;
		} else {
			final Attribute name = pkNode.getAttributeByName("name");
			final Attribute fieldName = pkNode.getAttributeByName("fieldName");

			final int counter = name == null ? 0 : 1;

			if (fieldName == null
					|| pkNode.getAttributes().size() != 1 + counter) {
				throw new CompilerException("");
			}

			final PrimaryKey pk = new PrimaryKey(name == null ? ""
					: name.getValue());
			pk.addField(fieldName.getValue());
			return pk;
		}
	}

	private Field readField(Node fieldNode) throws CompilerException {
		checkFieldNodeAttributes(fieldNode);

		final Field field = new Field(readFieldName(fieldNode),
				readFieldType(fieldNode));
		field.setUnique(readFieldUnique(fieldNode));
		field.setNotNull(readFieldNotNull(fieldNode));

		final List<Node> fkNodes = fieldNode.listChildrenByName("fk");

		if (fkNodes.size() > 1) {
			throw new CompilerException("Too many fk for field "
					+ field.getName());
		}
		if (fkNodes.size() != fieldNode.getChildren().size()) {
			throw new CompilerException("Incorrect children nodes in fk node");
		}

		if (fkNodes.size() == 1) {
			field.setForeignKey(readForeignKey(fkNodes.get(0)));
		}

		return field;
	}

	private void checkFieldNodeAttributes(Node fieldNode)
			throws CompilerException {
		for (Attribute att : fieldNode.getAttributes()) {
			if (!att.getName().equalsIgnoreCase("name")
					&& !att.getName().equalsIgnoreCase("type")
					&& !att.getName().equalsIgnoreCase("unique")
					&& !att.getName().equalsIgnoreCase("notNull")) {
				throw new CompilerException("");
			}
		}
	}

	private boolean readFieldNotNull(Node fieldNode) throws CompilerException {
		final List<Attribute> atts = fieldNode.listAttributesByName("notNull");
		if (atts.size() > 1) {
			throw new CompilerException(
					"Incorrect count of notNull attributes.");
		}
		if (atts.size() == 0)
			return false;

		return atts.get(0).getValue().equalsIgnoreCase("yes");
	}

	private boolean readFieldUnique(Node fieldNode) throws CompilerException {
		final List<Attribute> atts = fieldNode.listAttributesByName("unique");
		if (atts.size() > 1) {
			throw new CompilerException("Incorrect count of unique attributes.");
		}
		if (atts.size() == 0)
			return false;

		return atts.get(0).getValue().equalsIgnoreCase("yes");
	}

	private ForeignKey readForeignKey(Node foreignKeyNode)
			throws CompilerException {
		if (foreignKeyNode.getChildren().size() != 0) {
			throw new CompilerException("Incorrect count of fk children");
		}

		final Attribute name = foreignKeyNode.getAttributeByName("name");
		final Attribute refTable = foreignKeyNode
				.getAttributeByName("refTable");
		final Attribute refField = foreignKeyNode
				.getAttributeByName("refField");

		int counter = name == null ? 0 : 1;
		if (refTable == null || refField == null
				|| (foreignKeyNode.getAttributes().size() != 2 + counter)) {
			throw new CompilerException("Incorrect fk attributes");
		}

		return new ForeignKey(name == null ? "" : name.getValue(),
				refTable.getValue(), refField.getValue());
	}

	private String readFieldType(Node fieldNode) throws CompilerException {
		final List<Attribute> types = fieldNode.listAttributesByName("type");
		if (types.size() != 1) {
			throw new CompilerException("Incorrect count of type attributes.");
		}

		final String value = types.get(0).getValue();

		if (!value.startsWith("@")) {
			return value;
		}

		FieldMask mask = fieldMasks.get(value.substring(1));

		if (mask == null) {
			throw new CompilerException(value.substring(1)
					+ " - incorrect field mask id");
		}

		return mask.getMask();
	}

	private String readFieldName(Node fieldNode) throws CompilerException {
		final List<Attribute> names = fieldNode.listAttributesByName("name");
		if (names.size() != 1) {
			throw new CompilerException("Incorrect count of name attributes.");
		}
		return names.get(0).getValue();
	}

	private String readTableName(Node tableNode) throws CompilerException {
		if (tableNode.getAttributes().size() > 0) {
			if (tableNode.getAttributes().size() != 1
					|| !tableNode.getAttributes().get(0).getName()
							.equalsIgnoreCase("name")) {
				throw new CompilerException("Incorrect table attributes");
			}
			return tableNode.getAttributes().get(0).getValue();
		}

		throw new CompilerException("Table cannot be unnamed");
	}

	/**
	 * Zwraca nazwę bazy danych.
	 * 
	 * @param databaseNode
	 *            Node <i>database</i>
	 * @return Nazwa bazy danych lub "" jeśli nie ma takiego atrybutu
	 * @throws CompilerException
	 *             Rzucany jeśli węzeł ma niepoprawne atrybuty.
	 */
	private String readDatabaseName(Node databaseNode) throws CompilerException {
		if (databaseNode.getAttributes().size() > 0) {
			if (databaseNode.getAttributes().size() != 1
					|| !databaseNode.getAttributes().get(0).getName()
							.equalsIgnoreCase("name")) {
				throw new CompilerException("Incorrect database attributes");
			}
			return databaseNode.getAttributes().get(0).getValue();
		}

		return "";
	}
}
