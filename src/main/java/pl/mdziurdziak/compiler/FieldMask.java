package pl.mdziurdziak.compiler;

/**
 * @author Mateusz Dziurdziak
 */
class FieldMask {

	private String id;
	private String mask;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

}
