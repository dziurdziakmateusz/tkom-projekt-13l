package pl.mdziurdziak.compiler;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.parser.Node;

/**
 * @author Mateusz Dziurdziak
 */
public interface Compiler {
	public Database compile(Node schemaNode) throws CompilerException;
}
