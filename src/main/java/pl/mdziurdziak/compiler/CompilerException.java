package pl.mdziurdziak.compiler;

/**
 * Klasa wyjątku rzucanego przez kompilator w trakcie przetwarzania drzewa
 * zbudowanego przez parser.
 * 
 * @author Mateusz Dziurdziak
 */
public class CompilerException extends Exception {
	public CompilerException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 4389097130369402591L;
}
