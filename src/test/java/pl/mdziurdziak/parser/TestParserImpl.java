package pl.mdziurdziak.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;

import org.junit.Test;

import pl.mdziurdziak.parser.ParserImpl;
import pl.mdziurdziak.tokenizer.Token;
import pl.mdziurdziak.tokenizer.Tokenizer;

import static org.mockito.Mockito.*;
import static pl.mdziurdziak.tokenizer.TokenIdentifier.*;

public class TestParserImpl {

	@Test(expected = NullPointerException.class)
	public void testParseNull() throws IOException {
		final ParserImpl parserImpl = new ParserImpl();
		try {
			parserImpl.parse(null);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testBasicParse() throws IOException, ParseException {
		final Tokenizer tokenizer = mock(Tokenizer.class);
		when(tokenizer.getNextToken()).thenReturn(new Token(LT, "<"),
				new Token(SIMPLE_STRING, "element"), new Token(SPACE, " "),
				new Token(ONE_LINE_CLOSE, "/>"), new Token(EOF, ""));

		final Parser parser = new ParserImpl();
		final Node result = parser.parse(tokenizer);

		assertEquals("element", result.getName());
		assertEquals(0, result.getAttributes().size());
		assertEquals(0, result.getChildren().size());
	}

	@Test(expected = ParseException.class)
	public void testElementWithoutEnd() throws IOException, ParseException {
		final Tokenizer tokenizer = mock(Tokenizer.class);
		when(tokenizer.getNextToken()).thenReturn(new Token(LT, "<"),
				new Token(SIMPLE_STRING, "element"), new Token(EOF, ""));

		final Parser parser = new ParserImpl();
		parser.parse(tokenizer);
	}

	@Test
	public void testParseAttributes() throws IOException, ParseException {
		final Tokenizer tokenizer = mock(Tokenizer.class);
		when(tokenizer.getNextToken()).thenReturn(new Token(LT, "<"),
				new Token(SIMPLE_STRING, "element"), new Token(SPACE, " "),
				new Token(SIMPLE_STRING, "attrib"), new Token(EQUALS, "="),
				new Token(QUOTATION, "\""),
				new Token(SIMPLE_STRING, "attributeValue"),
				new Token(QUOTATION, "\""), new Token(SPACE, " "),
				new Token(SIMPLE_STRING, "secondAttribute"),
				new Token(EQUALS, "="), new Token(QUOTATION, "\""),
				new Token(SIMPLE_STRING, "sec"),
				new Token(QUOTATION_STRING, "&qout"),
				new Token(CHARACTER, "*"), new Token(SPACE, " "),
				new Token(SIMPLE_STRING, "end"), new Token(QUOTATION, "\""),
				new Token(SPACE, " "), new Token(ONE_LINE_CLOSE, "/>"),
				new Token(EOF, ""));

		final Parser parser = new ParserImpl();
		final Node result = parser.parse(tokenizer);

		assertEquals("element", result.getName());
		assertEquals(2, result.getAttributes().size());
		assertEquals(0, result.getChildren().size());
		assertEquals("attrib", result.getAttributes().get(0).getName());
		assertEquals("attributeValue", result.getAttributes().get(0).getValue());
		assertEquals("secondAttribute", result.getAttributes().get(1).getName());
		assertEquals("sec\"* end", result.getAttributes().get(1).getValue());
	}
	
	@Test
	public void testNestedElement() throws IOException, ParseException
	{
		final Tokenizer tokenizer = mock(Tokenizer.class);
		when(tokenizer.getNextToken()).thenReturn(new Token(LT, "<"),
				new Token(SIMPLE_STRING, "element"), new Token(GT, ">"),
				new Token(SPACE, " "), new Token(LT, "<"), new Token(SIMPLE_STRING, "nested"),
				new Token(SPACE, " "), new Token(ONE_LINE_CLOSE, "/>"), new Token(CLOSE, "</"),
				new Token(SIMPLE_STRING, "element"), new Token(GT, ">"), new Token(EOF, ""));

		final Parser parser = new ParserImpl();
		final Node result = parser.parse(tokenizer);

		assertEquals("element", result.getName());
		assertEquals(0, result.getAttributes().size());
		assertEquals(1, result.getChildren().size());
		assertEquals("nested", result.getChildren().get(0).getName());
		assertEquals(0, result.getChildren().get(0).getAttributes().size());
		assertEquals(0, result.getChildren().get(0).getChildren().size());
	}
}
