package pl.mdziurdziak.compiler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.databaseStructure.Table;
import pl.mdziurdziak.parser.Attribute;
import pl.mdziurdziak.parser.Node;

public class TestCompilerImpl {

	@Test(expected = NullPointerException.class)
	public void testCompileNull() throws CompilerException {
		final Compiler compiler = new CompilerImpl();
		compiler.compile(null);
	}

	@Test(expected = CompilerException.class)
	public void testSchemaWithoutDatabase() throws CompilerException {
		final Node schemaNode = new Node("schema");
		final Compiler compiler = new CompilerImpl();
		compiler.compile(schemaNode);
	}

	@Test
	public void testEmptyDatabase() throws CompilerException {
		final Node schemaNode = new Node("schema");
		final Node databaseNode = new Node("database");
		schemaNode.addChild(databaseNode);
		final Compiler compiler = new CompilerImpl();
		final Database database = compiler.compile(schemaNode);

		assertEquals("", database.getName());
		assertEquals(0, database.getTables().size());
	}

	@Test
	public void testEmptyNamedDatabase() throws CompilerException {
		final Node schemaNode = new Node("schema");
		final Node databaseNode = new Node("database");
		databaseNode.addAttribute(new Attribute("name", "databaseName"));
		schemaNode.addChild(databaseNode);
		final Compiler compiler = new CompilerImpl();
		final Database database = compiler.compile(schemaNode);

		assertEquals("databaseName", database.getName());
		assertEquals(0, database.getTables().size());
	}

	@Test
	public void testSimpleDatabase() throws CompilerException {
		final Node schemaNode = new Node("schema");

		final Node databaseNode = new Node("database");
		databaseNode.addAttribute(new Attribute("name", "databaseName"));
		schemaNode.addChild(databaseNode);

		final Node firstTable = new Node("table");
		firstTable.addAttribute(new Attribute("name", "FIRST_TABLE"));
		final Node firstField = new Node("field");
		firstField.addAttribute(new Attribute("name", "firstField"));
		firstField.addAttribute(new Attribute("type", "int"));
		firstTable.addChild(firstField);
		final Node secondField = new Node("field");
		secondField.addAttribute(new Attribute("name", "secondField"));
		secondField.addAttribute(new Attribute("type", "varchar(255)"));
		firstTable.addChild(secondField);
		databaseNode.addChild(firstTable);

		final Node secondTable = new Node("table");
		secondTable.addAttribute(new Attribute("name", "SECOND_TABLE"));
		final Node thirdField = new Node("field");
		thirdField.addAttribute(new Attribute("name", "thirdField"));
		thirdField.addAttribute(new Attribute("type", "varchar(100)"));
		thirdField.addAttribute(new Attribute("unique", "yes"));
		thirdField.addAttribute(new Attribute("notNull", "yes"));
		secondTable.addChild(thirdField);
		databaseNode.addChild(secondTable);

		final Compiler compiler = new CompilerImpl();
		final Database database = compiler.compile(schemaNode);

		assertEquals("databaseName", database.getName());
		assertEquals(2, database.getTables().size());
		// trzeba zrobic petle bo nie wiadomo w jakiej kolejnosci wystepuja
		// tabele
		for (Table t : database.getTables()) {
			if (t.getName().equalsIgnoreCase("FIRST_TABLE")) {
				assertEquals(2, t.getFields().size());
				// podobnie jak wyzej - nie wiadomo w jakiej kolejnosci wystapia
				for (Field f : t.getFields()) {
					if (f.getName().equalsIgnoreCase("firstField")) {
						assertEquals("int", f.getType());
					} else if (f.getName().equalsIgnoreCase("secondField")) {
						assertEquals("varchar(255)", f.getType());
					} else {
						assertTrue(false);
					}
				}
			} else if (t.getName().equalsIgnoreCase("SECOND_TABLE")) {
				assertEquals(1, t.getFields().size());
				assertEquals("thirdField", t.getFields().get(0).getName());
				assertEquals("varchar(100)", t.getFields().get(0).getType());
				assertTrue(t.getFields().get(0).isUnique());
				assertTrue(t.getFields().get(0).isNotNull());
			} else {
				// jeśli wyleci tutaj oznacza to ze zle odczytal tabele
				assertTrue(false);
			}
		}
	}

	@Test
	public void testMasks() throws CompilerException {
		final Node schemaNode = new Node("schema");

		final Node iMaskNode = new Node("fieldMask");
		iMaskNode.addAttribute(new Attribute("id", "I"));
		iMaskNode.addAttribute(new Attribute("mask", "int"));
		schemaNode.addChild(iMaskNode);

		final Node dMaskNode = new Node("fieldMask");
		dMaskNode.addAttribute(new Attribute("id", "D"));
		dMaskNode.addAttribute(new Attribute("mask", "varchar(255)"));
		schemaNode.addChild(dMaskNode);

		final Node databaseNode = new Node("database");
		databaseNode.addAttribute(new Attribute("name", "databaseName"));

		final Node firstTable = new Node("table");
		firstTable.addAttribute(new Attribute("name", "TABLE"));
		final Node firstField = new Node("field");
		firstField.addAttribute(new Attribute("name", "firstField"));
		firstField.addAttribute(new Attribute("type", "@I"));
		firstTable.addChild(firstField);
		final Node secondField = new Node("field");
		secondField.addAttribute(new Attribute("name", "secondField"));
		secondField.addAttribute(new Attribute("type", "@D"));
		firstTable.addChild(secondField);
		databaseNode.addChild(firstTable);

		schemaNode.addChild(databaseNode);

		final Compiler compiler = new CompilerImpl();
		final Database database = compiler.compile(schemaNode);

		assertEquals(1, database.getTables().size());
		final Table first = (Table) database.getTables().toArray()[0];
		assertEquals(2, first.getFields().size());
		for (Field f : first.getFields()) {
			if (f.getName().equalsIgnoreCase("firstField")) {
				assertEquals("int", f.getType());
			} else if (f.getName().equalsIgnoreCase("secondField")) {
				assertEquals("varchar(255)", f.getType());
			} else {
				assertTrue(false);
			}
		}
	}

	@Test
	public void testPrimaryKey() throws CompilerException {
		final Node schemaNode = new Node("schema");

		final Node databaseNode = new Node("database");
		databaseNode.addAttribute(new Attribute("name", "databaseName"));
		schemaNode.addChild(databaseNode);

		final Node firstTable = new Node("table");
		firstTable.addAttribute(new Attribute("name", "FIRST_TABLE"));
		final Node firstField = new Node("field");
		firstField.addAttribute(new Attribute("name", "firstField"));
		firstField.addAttribute(new Attribute("type", "int"));
		firstTable.addChild(firstField);
		final Node secondField = new Node("field");
		secondField.addAttribute(new Attribute("name", "secondField"));
		secondField.addAttribute(new Attribute("type", "varchar(255)"));
		firstTable.addChild(secondField);
		final Node primaryKeyNode = new Node("pk");
		primaryKeyNode.addAttribute(new Attribute("name", "PK"));
		primaryKeyNode.addAttribute(new Attribute("fieldName", "firstField"));
		firstTable.addChild(primaryKeyNode);

		databaseNode.addChild(firstTable);
		final Compiler compiler = new CompilerImpl();
		final Database database = compiler.compile(schemaNode);
		final Table first = (Table) database.getTables().toArray()[0];
		assertEquals(1, database.getTables().size());
		assertEquals(2, first.getFields().size());
		assertFalse(first.getPrimaryKey() == null);
		assertEquals("PK", first.getPrimaryKey()
				.getName());
		assertEquals(1, first.getPrimaryKey().getFields()
				.size());
		assertEquals("firstField", first.getPrimaryKey()
				.getFields().get(0));
	}
}
