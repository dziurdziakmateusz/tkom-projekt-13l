package pl.mdziurdziak.tokenizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.Test;

import pl.mdziurdziak.tokenizer.Token;
import pl.mdziurdziak.tokenizer.TokenIdentifier;
import pl.mdziurdziak.tokenizer.TokenizerImpl;

import com.google.common.base.Joiner;

import static pl.mdziurdziak.tokenizer.TokenIdentifier.*;

public class TestTokenizerImpl {

	@Test
	public void testDefaultInputStream() {
		final TokenizerImpl tokenizerImpl = new TokenizerImpl();
		assertNull(tokenizerImpl.getInputStream());
	}

	@Test
	public void testSetAndGetInputStream() {
		final ByteArrayInputStream bais = new ByteArrayInputStream(
				"".getBytes());
		final TokenizerImpl tokenizerImpl = new TokenizerImpl();
		tokenizerImpl.setInputStream(bais);
		assertEquals(tokenizerImpl.getInputStream(), bais);
	}

	@Test(expected = IllegalStateException.class)
	public void testReadWithoutSetInputStream() throws IOException {
		final TokenizerImpl tokenizerImpl = new TokenizerImpl();
		tokenizerImpl.getNextToken();
	}

	@Test
	public void testTokenize() throws IOException {
		final ByteArrayInputStream bais = new ByteArrayInputStream(
				"<element>tekst </element>".getBytes());
		final TokenizerImpl tokenizerImpl = new TokenizerImpl();
		tokenizerImpl.setInputStream(bais);
		assertEquals(LT, tokenizerImpl.getNextToken().getId());
		assertEquals(SIMPLE_STRING, tokenizerImpl.getNextToken().getId());
		assertEquals(GT, tokenizerImpl.getNextToken().getId());
		assertEquals(SIMPLE_STRING, tokenizerImpl.getNextToken().getId());
		assertEquals(SPACE, tokenizerImpl.getNextToken().getId());
		assertEquals(CLOSE, tokenizerImpl.getNextToken().getId());
		assertEquals(SIMPLE_STRING, tokenizerImpl.getNextToken().getId());
		assertEquals(GT, tokenizerImpl.getNextToken().getId());
		assertEquals(EOF, tokenizerImpl.getNextToken().getId());
		assertEquals(EOF, tokenizerImpl.getNextToken().getId());
	}

	@Test
	public void testTokenizeCharacter() throws IOException {
		final String characters = "!@#%^&*(){}[];:,.?/\\-";
		final ByteArrayInputStream bais = new ByteArrayInputStream(
				characters.getBytes());
		final TokenizerImpl tokenizerImpl = new TokenizerImpl();
		tokenizerImpl.setInputStream(bais);
		for (int i = 0; i < characters.length(); i++) {
			final Token token = tokenizerImpl.getNextToken();
			assertEquals(CHARACTER, token.getId());
			assertEquals(""+characters.charAt(i), token.getValue());
		}
		assertEquals(tokenizerImpl.getNextToken().getId(), EOF);
	}

	@Test
	public void testTokenizeXmlMacros() throws IOException {
		final String[] characters = { "&amp", "&lt", "&gt", "&quot", "&apos" };
		final TokenIdentifier[] identifiers = { AMP_STRING, LT_STRING,
				GT_STRING, QUOTATION_STRING, APOS_STRING };
		test(characters, identifiers);
	}
	
	@Test
	public void testComments() throws IOException{
		final String[] characters = { "<", "elem", ">", "tekst", "<--", "comment", "-->", "</", "elem", ">" };
		final TokenIdentifier[] identifiers = { LT, SIMPLE_STRING,
				GT, SIMPLE_STRING, COMMENT_BEGIN, SIMPLE_STRING, COMMENT_END, CLOSE, SIMPLE_STRING, GT };
		test(characters, identifiers);
	}
	
	private void test(String[] characters, TokenIdentifier[] identifiers) throws IOException
	{
		try (final ByteArrayInputStream bais = new ByteArrayInputStream(Joiner
				.on("").join(characters).getBytes())) {
			final TokenizerImpl tokenizerImpl = new TokenizerImpl();
			tokenizerImpl.setInputStream(bais);

			for (int i = 0; i < characters.length; i++) {
				final Token token = tokenizerImpl.getNextToken();
				assertEquals(identifiers[i], token.getId());
				assertEquals(characters[i], token.getValue());
			}
			assertEquals(EOF, tokenizerImpl.getNextToken().getId());
		}
	}

}
