package pl.mdziurdziak.sqlCreator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;

import pl.mdziurdziak.databaseStructure.Database;
import pl.mdziurdziak.databaseStructure.Field;
import pl.mdziurdziak.databaseStructure.ForeignKey;
import pl.mdziurdziak.databaseStructure.PrimaryKey;
import pl.mdziurdziak.databaseStructure.Table;

public class TestMsSqlCreator {

	@Test(expected = NullPointerException.class)
	public void testCheckNull() throws LogicException {
		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(null);
	}

	@Test(expected = LogicException.class)
	public void testCheckEmptyTable() throws LogicException {
		final Database database = new Database("test");
		database.getTables().add(new Table("empty"));

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test(expected = LogicException.class)
	public void testCheckTablesWithDuplicateNames() throws LogicException {
		final Database database = new Database("test");
		final Table first = new Table("duplicate");
		first.addField(new Field("field", "int"));
		database.getTables().add(first);

		final Table second = new Table("duplicate");
		second.addField(new Field("field", "int"));
		database.getTables().add(second);

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test(expected = LogicException.class)
	public void testCheckTableWithDuplicateFields() throws LogicException {
		final Database database = new Database("test");
		final Table tab = new Table("tab");
		tab.addField(new Field("field", "int"));
		tab.addField(new Field("field", "int"));
		database.getTables().add(tab);

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test(expected = LogicException.class)
	public void testCheckTableWithKeywordName() throws LogicException {
		final Database database = new Database("test");
		final Table tab = new Table("CREATE");
		tab.addField(new Field("field", "int"));
		database.getTables().add(tab);

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test
	public void testCheckSimpleDatabse() throws LogicException {
		final Database database = new Database("test");
		final Table first = new Table("first");
		first.addField(new Field("field", "int"));
		database.getTables().add(first);

		final Table second = new Table("second");
		second.addField(new Field("field", "int"));
		database.getTables().add(second);

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test
	public void testCheckForeignKey() throws LogicException {
		final Database database = new Database("test");
		final Table first = new Table("first");
		final Field f = new Field("field", "int");
		f.setForeignKey(new ForeignKey("", "second", "field"));
		first.addField(f);
		database.getTables().add(first);

		final Table second = new Table("second");
		second.addField(new Field("field", "int"));
		database.getTables().add(second);

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test(expected = LogicException.class)
	public void testCheckIncorrectForeignKey() throws LogicException {
		final Database database = new Database("test");
		final Table first = new Table("first");
		final Field f = new Field("field", "varchar(255)");
		f.setForeignKey(new ForeignKey("", "second", "field"));
		first.addField(f);
		database.getTables().add(first);

		final Table second = new Table("second");
		second.addField(new Field("field", "int"));
		database.getTables().add(second);

		final SqlCreator sqlCreator = new MsSqlCreator();
		sqlCreator.checkForErrors(database);
	}

	@Test
	public void testExport() throws LogicException, IOException {
		final Database database = new Database("test");
		final Table first = new Table("first");
		final Field f = new Field("field", "int");
		f.setUnique(true);
		f.setNotNull(true);
		f.setForeignKey( new ForeignKey("", "second", "field"));
		f.setForeignKey(new ForeignKey("", "second", "field"));
		final PrimaryKey pk = new PrimaryKey("");
		pk.addField("field");
		first.setPrimaryKey(pk);
		first.addField(f);
		database.getTables().add(first);

		final Table second = new Table("second");
		second.addField(new Field("field", "int"));
		database.getTables().add(second);

		final SqlCreator sqlCreator = new MsSqlCreator();

		final FileOutputStream fos = new FileOutputStream(new File("test.sql"));
		sqlCreator.write(fos, database);
		fos.close();
	}
}
